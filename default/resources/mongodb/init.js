use admin
db.createUser(
    {
        user: "mongo",
        pwd: "mongo",
        roles: [
            { role: "userAdminAnyDatabase", db: "admin" }
        ]
    }
)

use mongodb
db.createUser(
    {
        user: "mongo",
        pwd: "mongo",
        roles: [
            { role: "readWrite", db: "mongodb" }
        ]
    }
)

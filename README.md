## 1. Preparar el entorno local
** Windows (testado con WSL 2 en Windows 10 20H2) **:

- iniciar Docker Desktop
- iniciar el terminal de Ubuntu

** Importante: ** en Windows es necesario ejecturar todos los comandos desde el terminal de Ubuntu con usuario 'root'.

Si se pone la carpeta de trabajo en la carpeta del usuario de Windows, ésta será accesible desde el terminal de Ubuntu a través la ruta '/mnt/c/Users/NOMBRE_USUARIO/'.

Si nuestra carpeta de trabajo se llama 'www', la ruta completa será '/mnt/c/Users/NOMBRE_USUARIO/www'.
Es conveniente hacer un enlace simbolico a '/var/www' para facilitar el acceso desde la shell de Ubuntu.

```bash
$ sudo -s
$ ln -s /mnt/c/Users/NOMBRE_USUARIO/www /var/www
$ cd /var/www 
```

** Linux (testado en Debian y derivadas) **:

- iniciar el terminal

```bash
$ sudo mkdir /var/wwww
$ sudo chown -R $USER:$USER /var/www
```

### Construir la imagen de base (que será comportida con todos los proyectos)
```bash
$ cd /var/www/default
$ docker-compose build
```

## 2. Crear un nuevo proyecto
Para crear un nuevo proyecto Wordpress con nombre 'mydomain':

```bash
$ sh newproject mydomain wordpress
```

### Añadir certificado de seguridad en el navegador
Se debe importar el certificado de seguridad ubicado en `resources/certs/NOMBRE_PROYECTORootCA.crt` entre las Entidades Emisoras del navegador web y confiar en el certificado

- Chrome: `Configuración -> Privacidad & Seguridad -> Seguridad -> Gestionar certificados (importar en el tab Entidades Emisoras)`
- Firefox: `Preferencias -> Privacidad & Seguridad -> Ver certificados... -> Autoridades -> Importar`

### Iniciar el container del proyecto
```bash
$ cd /var/www/NOMBRE_PROYECTO
$ sh dockerup
```

Cuando ha terminado de bajar todas las imágenes de dependencias, el nuestro proyecto estará listo para la instalación del framework / CMS preferido 

### Añadir entrada en `hosts` del dominio
Modificar el archivo `hosts` del sistema operativo, ubicado en la ruta `C:\Windows\System32\Drivers\etc` (en Windows) o en `/etc` (en Linux), y añadir la entrada al dominio `NOMBRE_PROYECTO.test`

```
127.0.0.1 NOMBRE_PROYECTO.test
```

## Instalación de Wordress
Bajar la última versión de Wordpress desde aqui: `https://es.wordpress.org/latest-es_ES.zip` y decomprimir el archivo en la carpeta `public` del proyecto

Dirigerse en https://www.NOMBRE_PROYECTO.test y seguir con la instalación

## Acceso a Adminer
Dirigerse en http://localhost:8080/?server=mysql&username=root